TEMPLATE = app
TARGET = magellan
OBJECTS_DIR = obj/
MOC_DIR = obj/
DEPENDPATH += includes/
INCLUDEPATH += . \
    /usr/include/logram \
    includes/
LIBS += -llmisc \
    -llwidgets \
    -llio

# Input
HEADERS += includes/mwindow.h \
    includes/settingswindow.h \
    googlecompleter.h \
    toolbar.h 
SOURCES += main.cpp \
    mwindow.cpp \
    mainWindow/preferences.cpp \
    mainWindow/history.cpp \
    mainWindow/navigation.cpp \
    mainWindow/edition.cpp \
    mainWindow/downloads.cpp \
    mainWindow/settingswindow.cpp \
    googlecompleter.cpp \
    toolbar.cpp 
QT += webkit \
    network
target.path = /usr/bin/
config.files = Magellan.conf
config.path = /etc/xdg/Logram/
icons.files = icons
icons.path = /usr/share/logram/magellan/
CONFIG += qt \
    debug
INSTALLS += target \
    config \
    icons
