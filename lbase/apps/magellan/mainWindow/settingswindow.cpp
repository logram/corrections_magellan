/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "../includes/settingswindow.h"

SettingsWindow::SettingsWindow(const QSettings& settings, QWidget* parent) :
	QMainWindow(parent)
{
		setWindowFlags(Qt::Tool);
		setMaximumSize(650, 475);
		setWindowTitle(tr("Preferences du Navigateur Web Magellan"));

		mainWidget = new QTabWidget();



		// ##################
		//    General tab

		general = new QWidget;
		generalLayout = new QVBoxLayout();

                start = new QGroupBox(tr("Demarrage"));
			atStart = new QLabel(tr("Au lancement du navigateur : "));
			startComboBox = new QComboBox();
				startComboBox->addItem(tr("Afficher ma page d'accueil"));
				startComboBox->addItem(tr("Afficher une page blanche"));
				startComboBox->addItem(tr("Afficher les dernieres pages visitees"));

				startComboBox->setCurrentIndex(settings.value("startup").toInt());
                                connect(startComboBox, SIGNAL(currentIndexChanged(int)), this, SIGNAL(startChanged(int)));
			homeLabel = new QLabel(tr("Page d'accueil : "));

			homeLineEdit = new QLineEdit();
				homeLineEdit->setText(settings.value("home").toString());
                                connect(homeLineEdit, SIGNAL(textEdited(QString)), this, SIGNAL(homeChanged(QString)));

			homeCurrentPage = new QPushButton(tr("Page actuelle"));
                                connect(homeCurrentPage, SIGNAL(clicked()), this, SIGNAL(homeCurrentPageSelected()));

			homeBlankPage = new QPushButton(tr("Page blanche"));
                                connect(homeBlankPage, SIGNAL(clicked()), this, SIGNAL(homeBlankPageSelected()));

			homeDefaultPage = new QPushButton(tr("Par defaut"));
                        connect(homeDefaultPage, SIGNAL(clicked()), this, SIGNAL(homeDefaultPageSelected()));

		startLayout = new QGridLayout();
		font = new QFontComboBox();
                connect(font, SIGNAL(currentFontChanged(QFont)), this, SIGNAL(fontChanged(QFont)));

		startLayout->addWidget(atStart, 0, 0);
		startLayout->addWidget(startComboBox, 0, 1, 1, 3);
		startLayout->addWidget(homeLabel, 1, 0);
		startLayout->addWidget(homeLineEdit, 1, 1, 1, 3);
		startLayout->addWidget(homeCurrentPage, 2, 1);
		startLayout->addWidget(homeBlankPage, 2, 2);
		startLayout->addWidget(homeDefaultPage, 2, 3);
		startLayout->addWidget(font, 3, 0);

		start->setLayout(startLayout);

		generalLayout->addWidget(start);

		general->setLayout(generalLayout);

		mainWidget->addTab(general,tr("General"));


		// ##################
		//   connexion tab  
		connexion = new QWidget();
			connexionLayout = new QVBoxLayout();
				infoProxy = new QGroupBox();
				infoProxyLayout = new QFormLayout();
                                        hostname = new QLineEdit(settings.value("Proxy/hostname","").toString());
                                        connect(hostname,SIGNAL(textChanged(QString)),this,SIGNAL(proxyHostChanged(QString)));
					infoProxyLayout->addRow(tr("Hostname :"),hostname);
					
                                        port = new QLineEdit(settings.value("Proxy/port","").toString());
                                        connect(port,SIGNAL(textChanged(QString)),this,SIGNAL(proxyPortChanged(QString)));
					infoProxyLayout->addRow(tr("Port :"),port);

                                        user = new QLineEdit(settings.value("Proxy/username","").toString());
                                        connect(user,SIGNAL(textChanged(QString)),this,SIGNAL(proxyUserChanged(QString)));
					infoProxyLayout->addRow(tr("Username :"),user);

                                        pass = new QLineEdit(settings.value("Proxy/pass").toString());
                                        connect(pass,SIGNAL(textChanged(QString)),this,SIGNAL(proxyPassChanged(QString)));
					infoProxyLayout->addRow(tr("Password :"),pass);
				infoProxy->setLayout(infoProxyLayout);
				connexionLayout->addWidget(infoProxy);

				connexionType = new QGroupBox();
					connexionTypeLayout = new QHBoxLayout();
                                                noConn = new QRadioButton(tr("No Proxy"));
                                                if(settings.value("Proxy/type",QNetworkProxy::NoProxy).toInt() == QNetworkProxy::NoProxy)
                                                    noConn->setChecked(true);
                                                lastIsActivated = noConn->isChecked();
                                                connect(noConn,SIGNAL(toggled(bool)),this,SLOT(proxyTypeObserver()));
						connexionTypeLayout->addWidget(noConn);


						connHttp = new QRadioButton(tr("HTTP"));
                                                if(settings.value("Proxy/type",QNetworkProxy::NoProxy).toInt() == QNetworkProxy::HttpProxy)
                                                    connHttp->setChecked(true);
                                                connect(connHttp,SIGNAL(toggled(bool)),this,SLOT(proxyTypeObserver()));
						connexionTypeLayout->addWidget(connHttp);

						connSocks5 = new QRadioButton(tr("Socks 5"));
                                                if(settings.value("Proxy/type",QNetworkProxy::NoProxy).toInt() == QNetworkProxy::Socks5Proxy)
                                                    connSocks5->setChecked(true);
                                                connect(connSocks5,SIGNAL(toggled(bool)),this,SLOT(proxyTypeObserver()));
						connexionTypeLayout->addWidget(connSocks5);

						connHttpCaching = new QRadioButton(tr("Only HTTP"));
                                                if(settings.value("Proxy/type",QNetworkProxy::NoProxy).toInt() == QNetworkProxy::HttpCachingProxy)
                                                    connHttpCaching->setChecked(true);
                                                connect(connHttpCaching,SIGNAL(toggled(bool)),this,SLOT(proxyTypeObserver()));
						connexionTypeLayout->addWidget(connHttpCaching);

						connFtpCaching = new QRadioButton(tr("Only FTP"));
                                                if(settings.value("Proxy/type",QNetworkProxy::NoProxy).toInt() == QNetworkProxy::FtpCachingProxy)
                                                    connFtpCaching->setChecked(true);
                                                connect(connFtpCaching,SIGNAL(toggled(bool)),this,SLOT(proxyTypeObserver()));
						connexionTypeLayout->addWidget(connFtpCaching);
					connexionType->setLayout(connexionTypeLayout);
					connexionLayout->addWidget(connexionType);
		connexion->setLayout(connexionLayout);
		mainWidget->addTab(connexion, tr("Connexion"));

		setCentralWidget(mainWidget);
		
}
void SettingsWindow::fontDialog() 
{

}

void SettingsWindow::proxyTypeObserver() {

    if(noConn!=NULL && noConn->isChecked()!=lastIsActivated)
    {
        lastIsActivated = noConn->isChecked();
        // TODO : make "No Proxy" a Checkbox
        emit(useProxyChanged(!noConn->isChecked()));

        emit(proxyTypeChanged(currentProxyType()));
    }


    emit(proxyTypeChanged(currentProxyType()));
}

QNetworkProxy::ProxyType SettingsWindow::currentProxyType() {
    if(connHttp==NULL || connSocks5==NULL || connHttpCaching==NULL || connFtpCaching==NULL)
        return QNetworkProxy::NoProxy;
    if(connHttp->isChecked())
        return QNetworkProxy::HttpProxy;
    if(connSocks5->isChecked())
        return QNetworkProxy::Socks5Proxy;
    if(connHttpCaching->isChecked())
        return QNetworkProxy::HttpCachingProxy;
    if(connHttp->isChecked())
        return QNetworkProxy::FtpCachingProxy;

    return QNetworkProxy::NoProxy;

}
