#include "mwindow.h"

/* file providing overloaded high-level functions */

void mwindow::back() {
    currentPage()->back(); }
void mwindow::forward() {
    currentPage()->forward(); }
void mwindow::refresh() {
    currentPage()->reload(); }
void mwindow::stop() {
    currentPage()->stop(); }
void mwindow::home() {
    open(settings->value("Settings/home").toString());  }
void mwindow::loadingStarted() {
    toolbar->setValue(0); }
void mwindow::loadingProgress(int value) {
    toolbar->setValue(value); 
    QIcon ic = QWebSettings::iconForUrl(currentPage()->url());
    if(ic.isNull())
        ic = QIcon();
    QPixmap pix = ic.pixmap(32, 32);
    QTransform trans;
    trans.rotate(90);
    trans.scale(32, 32);
    QIcon icon(pix.transformed(trans));
    tabs->setTabIcon(tabs->currentIndex(),icon);
    changeTitle(currentPage()->title());
}
void mwindow::loadingFinished(bool ok) {
    toolbar->setValue(0);
    if(!ok)
        QMessageBox::critical(this,tr("Error"),tr("Error trying to access page"));
}
QUrl mwindow::parseUrl(QUrl url)
{
    if(url.toString().startsWith("http://", Qt::CaseSensitive));
    else if(url.toString().startsWith("https://", Qt::CaseSensitive));
    else if(url.toString().startsWith("ftp://", Qt::CaseSensitive));
    else if(url.toString().startsWith("file://",Qt::CaseSensitive));
    else if(url.toString().startsWith("www", Qt::CaseSensitive) || (!url.toString().contains(' ') && url.toString().contains('.'))) url = QUrl("http://" + url.toString());
    else url = QUrl("http://www.google.fr/search?hl=fr&q=" + url.toString() + "&btnG=Recherche+Google&meta=&aq=f&oq=");

    return url;
}
