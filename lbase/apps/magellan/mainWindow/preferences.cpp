// in this file are declared all methods related to the personnalisation window
#include "mwindow.h"

void mwindow::defineStart(int index)
{
	settings->setValue("startup", index); 
}

void mwindow::defineHome(QString home)
{
	settings->setValue("home", home);
}

void mwindow::defineHomeBlankPage()
{
        defineHome("");
}

void mwindow::defineHomeDefaultPage()
{
        defineHome("http://titouille56.hostei.com/html/2009/09/logram_welcome/");
}

void mwindow::defineHomeCurrentPage()
{
        defineHome(currentPage()->url().toString());

}

void mwindow::defineFont(QFont font)
{
}

void mwindow::fontDialog()
{
    QFontDialog *fontDialog = new QFontDialog();
}

void mwindow::defineDownloadDir(QString dir) {
    downloadDir = dir;
}


void mwindow::defineUseProxy(bool active) {
    if(!active) {
	networkProxy->setType(QNetworkProxy::NoProxy);
        wstatusBar->showMessage(tr("Proxy desactivated."),2000);
    }

    else
    wstatusBar->showMessage(tr("Proxy activated."),2000);
}

void mwindow::defineProxyType(QNetworkProxy::ProxyType type) {
    networkProxy->setType(type);
    settings->setValue("Proxy/type", static_cast<int>(type));
}

void mwindow::defineProxyHost(QString hostname) {
    networkProxy->setHostName(hostname);
    settings->setValue("Proxy/hostname", hostname);
    wstatusBar->showMessage(tr("New proxy hostname is %1.").arg(hostname),2000);
}

void mwindow::defineProxyPort(QString port) {
    networkProxy->setPort(port.toInt());
    settings->setValue("Proxy/port",port);
    wstatusBar->showMessage(tr("New proxy port is %1.").arg(port),2000);
}

void mwindow::defineProxyUser(QString user) {
    networkProxy->setUser(user);
    settings->setValue("Proxy/username", user);
    wstatusBar->showMessage(tr("New proxy user is %1.").arg(user),2000);
}

void mwindow::defineProxyPass(QString pass) {
    // TODO use a wallet instead of save the password as this.
    networkProxy->setPassword(pass);
    settings->setValue("Proxy/password",pass);
    wstatusBar->showMessage(tr("Proxy password has been changed."),2000);
}


void mwindow::openSettings()
{
	wsettings->show();
}

