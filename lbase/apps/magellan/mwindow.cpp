#include "mwindow.h"
#include <LConfig.h>
#include "toolbar.h"

mwindow::mwindow()
{
    // creation d'un QSettings et recuperation du theme
    settings = new QSettings("Logram", "Magellan");

    iconPath = "/usr/share/logram/magellan/icons/";

    // restauration de la fenetre
    resize(settings->value("MainWindow/width").toInt(), settings->value("MainWindow/height").toInt());
    if(settings->value("MainWindow/isMaximized").toBool()) {}
    else { move(settings->value("MainWindow/positionX").toInt(), settings->value("MainWindow/positionY").toInt()); }
    setWindowTitle(tr("Navigateur web Magellan"));
    setWindowIcon(QIcon(iconPath+"main.png"));



    wstatusBar = this->statusBar();


    // construction de la barre d'outils
    toolbar = new ToolBar();
      connect(toolbar,SIGNAL(settings()),this,SLOT(openSettings()));
      addToolBar(static_cast<Qt::ToolBarArea>(settings->value("Toolbars/AdressBarArea"),Qt::TopToolBarArea),toolbar);

    // construction du systeme d'onglets
    tabs = new QTabBar;
    QObject::connect(tabs,SIGNAL(tabCloseRequested(int)), this, SLOT(closeTab(int)));
    tabsDock = new QToolBar(this);
        newTabAction = new QAction(QIcon(iconPath+"add.png"),tr(""),this);
            newTabAction->setShortcut(QKeySequence("Ctrl+T"));
            tabsDock->addAction(newTabAction);
        tabsDock->addWidget(tabs);
        closeTabAction = new QAction(QIcon(iconPath+"quit.png"),tr(""),this);
            closeTabAction->setShortcut(QKeySequence("Ctrl+W"));
            tabsDock->addAction(closeTabAction);
        addToolBar(static_cast<Qt::ToolBarArea>(settings->value("Toolbars/TabbarArea"),Qt::TopToolBarArea),tabsDock);
    tabsContents = new QStackedWidget(this);
    QObject::connect(tabs,SIGNAL(currentChanged(int)),this,SLOT(changeTab(int)));

    networkProxy = new QNetworkProxy();
        defineUseProxy(static_cast<QNetworkProxy::ProxyType>(settings->value("Proxy/type",QNetworkProxy::NoProxy).toInt())!=QNetworkProxy::NoProxy);
        defineProxyType(static_cast<QNetworkProxy::ProxyType>(settings->value("Proxy/type",QNetworkProxy::NoProxy).toInt()));
        defineProxyHost(settings->value("Proxy/hostname","").toString());
        defineProxyPort(settings->value("Proxy/port","8080").toString());
        defineProxyUser(settings->value("Proxy/username","").toString());
        defineProxyPass(settings->value("Proxy/password","").toString());

        QNetworkProxy::setApplicationProxy(*networkProxy);

    setCentralWidget(tabsContents);
    
    // recuperation des parametres de demarrage
    int startup = settings->value("MainWindow/startup").toInt();
    switch (startup) {
        case 0 :
            this->home(); // 0 : ouvrir la page d'accueil
            break;
            
        case 1 :
            this->home();
            break;
            
        case 2 :
            readSavedTabs();
            if (tabs->count() < 1) {
                //tabs->makeTab(QUrl(""),""); 
                this->home(); // 2 : ouvrir les derniers onglets
            }
            break;
            
        default :
            openTab(true,"Home",QIcon::fromTheme("Home")); 
            this->home(); // si erreur dans le fichier de conf, on ouvre la page d'accueil
	    break; }

    // creation des parametres web
    if (!QDir(qApp->applicationDirPath() + "/cache/").exists()) {
        QDir(qApp->applicationDirPath()).mkdir("cache"); }

    QWebSettings::setIconDatabasePath(qApp->applicationDirPath() + "/cache/");



    wsettings = new SettingsWindow(*settings,this);
        //General
        connect(wsettings,SIGNAL(homeChanged(QString)),this,SLOT(defineHome(QString)));
        connect(wsettings,SIGNAL(homeCurrentPageSelected()),this,SLOT(defineHomeCurrentPage()));
        connect(wsettings,SIGNAL(homeBlankPageSelected()),this,SLOT(defineHomeBlankPage()));
        connect(wsettings,SIGNAL(homeDefaultPageSelected()),this,SLOT(defineHomeDefaultPage()));
        connect(wsettings,SIGNAL(fontChanged(QFont)),this,SLOT(defineFont(QFont)));
        connect(wsettings,SIGNAL(downloadDirChanged(QString)),this,SLOT(defineDownloadDir(QString)));

        //Connexion
        connect(wsettings,SIGNAL(proxyHostChanged(QString)),this,SLOT(defineProxyHost(QString)));
        connect(wsettings,SIGNAL(proxyPassChanged(QString)),this,SLOT(defineProxyPass(QString)));
        connect(wsettings,SIGNAL(proxyPortChanged(QString)),this,SLOT(defineProxyPort(QString)));
        connect(wsettings,SIGNAL(proxyTypeChanged(QNetworkProxy::ProxyType)),this,SLOT(defineProxyType(QNetworkProxy::ProxyType)));
        connect(wsettings,SIGNAL(proxyUserChanged(QString)),this,SLOT(defineProxyUser(QString)));
        connect(wsettings,SIGNAL(useProxyChanged(bool)),this,SLOT(defineUseProxy(bool)));

    connect(toolbar,SIGNAL(go()),this,SLOT(openUrl()));



    connect(newTabAction,SIGNAL(triggered()),this,SLOT(openTab()));
    connect(closeTabAction,SIGNAL(triggered()),this,SLOT(closeCurrent()));

    wstatusBar->showMessage(tr("Ready"));

}

QWebView *mwindow::currentPage()
{
    return tabsContents->currentWidget()->findChild<QWebView*>();
}

void mwindow::changeTitle(const QString & title)
{
    toolbar->setText(title);
    tabs->setTabText(tabs->currentIndex(),title);
}

void mwindow::changeUrl(const QUrl & url)
{
    toolbar->setUrl(url);
}

void mwindow::changeTab(int)
{
    if(currentPage() == NULL)
        return;
    changeTitle(currentPage()->title());
    changeUrl(currentPage()->title());
}

void mwindow::closeTab(int index) {
    tabs->removeTab(index);
    tabsContents->removeWidget(tabsContents->widget(index));
}

void mwindow::closeCurrent() {
    closeTab(tabs->currentIndex());
}

void mwindow::newWin()
{
}

/* files management slots */

void mwindow::open(QUrl url) 
{
    if(currentPage() == NULL)
        openTab(true);
    
    url = parseUrl(url);
    toolbar->setUrl(url);
    currentPage()->load(url);
        
       
    
}

void mwindow::openUrl()
{
    open(toolbar->url());
}

void mwindow::openTab(bool needswitch,const QString& title, const QIcon& icon) 
{
    int index = tabs->addTab(icon,title);
    QWebView* view = new QWebView(this);
    QVBoxLayout* viewLayout = new QVBoxLayout(this);
    QWidget* viewWidget = new QWidget(this);
        viewLayout->addWidget(view);
        viewWidget->setLayout(viewLayout);
        view->page()->networkAccessManager()->setProxy(*networkProxy);
        
    connect(view,SIGNAL(loadStarted()),this,SLOT(loadingStarted()));
    connect(view,SIGNAL(loadProgress(int)),this,SLOT(loadingProgress(int)));
    connect(view,SIGNAL(loadFinished(bool)),this,SLOT(loadingFinished(bool)));
    connect(view,SIGNAL(statusBarMessage(QString)),wstatusBar,SLOT(showMessage(QString)));
    tabsContents->insertWidget(index,viewWidget);
    if(needswitch) {
        tabs->setCurrentIndex(index);
        tabsContents->setCurrentWidget(viewWidget);
    }
}


void mwindow::about()
{
}

void mwindow::aboutLogram()
{
}

void mwindow::closeEvent(QCloseEvent *event)
{
    // while closing - save the size and the position of the main window
    settings->setValue("MainWindow/height", this->height());
    settings->setValue("MainWindow/width", this->width());
    settings->setValue("MainWindow/isMaximized", this->isMaximized());
    settings->setValue("MainWindow/positionX", this->y());
    settings->setValue("MainWindow/positionY", this->x());
    int startup = settings->value("startup").toInt();
    if (startup == 2) {
        int tabsNumber = tabs->count();
        int tabIndex = 0;
        QString tab = "0";
        settings->remove("SavedTabs");
        while (tabIndex < tabsNumber) {
            tabs->setCurrentIndex(tabIndex);
            QString tabUrl = currentPage()->url().toString();
            settings->setValue("SavedTabs/tabsNumber", tabsNumber);
            settings->setValue("SavedTabs/tabIndex", tabIndex);
            tab = "tab" + settings->value("SavedTabs/tabIndex").toString();
            settings->setValue("SavedTabs/tab" + tab, tabUrl);
            settings->remove("SavedTabs/tabIndex");
            tabIndex++;
        }
    }
    else { settings->remove("SavedTabs"); }
    settings->setValue("Toolbars/AdressBarArea",this->toolBarArea(toolbar));
    settings->setValue("Toolbars/TabbarArea",this->toolBarArea(tabsDock));
    event->accept();
}

void mwindow::readSavedTabs()
{
    int tabsNumber = settings->value("SavedTabs/tabsNumber").toInt();
    int tabIndex = 0;
    QString tab = "0";
    while (tabIndex < tabsNumber) {
        settings->setValue("SavedTabs/tabsNumber", tabsNumber);
        settings->setValue("SavedTabs/tabIndex", tabIndex);
        tab = "tab" + settings->value("SavedTabs/tabIndex").toString();
        QString url = settings->value("SavedTabs/tab" + tab).toString();
        openTab(true);
        open(url);
        tabIndex++;
    }
}

