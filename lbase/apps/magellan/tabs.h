#ifndef TABS_H
#define TABS_H

#include <QTabWidget>
#include <QWebView>


class mwindow;

class Tabs : public QTabBar
{
public:
    Tabs(mwindow* parent);
    QWebView *page();
public slots:
    void makeTab(QUrl,QString);
    void main(int);
    void mainW();
private:
    mwindow* mainWindow;
   };

#endif // TABS_H
