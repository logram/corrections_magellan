/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef SETTINGS_WINDOW_H
#define SETTINGS_WINDOW_H

#include <QNetworkProxy>

#include <QSettings>

#include <QMainWindow>
#include <QWidget>
#include <QTabWidget>
#include <QGroupBox>
#include <QLabel>
#include <QComboBox>
#include <QLineEdit>
#include <QPushButton>
#include <QFontComboBox>
#include <QRadioButton>

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QFormLayout>

class SettingsWindow : public QMainWindow
{
    Q_OBJECT
    public:
	SettingsWindow(const QSettings& settings,QWidget* parent=NULL);

    public:
                QNetworkProxy::ProxyType currentProxyType();

    private slots:
		void fontDialog();
                void proxyTypeObserver();


    signals:
		void startChanged(int index);
		void homeChanged(QString home);
                void homeCurrentPageSelected();
                void homeDefaultPageSelected();
                void homeBlankPageSelected();
		
		void fontChanged(QFont);
		void downloadDirChanged(QString);

		// CONNEXION TAB : SLOTS
                void useProxyChanged(bool);
                void proxyTypeChanged(QNetworkProxy::ProxyType);
                void proxyHostChanged(QString);
		void proxyPortChanged(QString);
		void proxyUserChanged(QString);
		void proxyPassChanged(QString);
	private:
		QTabWidget* mainWidget;
	
                // proxy
                bool lastIsActivated;

		QWidget* general;
			QVBoxLayout* generalLayout;

				QGroupBox* start;
					QGridLayout* startLayout;
					QLabel* atStart;
					QComboBox* startComboBox;
					QLabel* homeLabel;
					QLineEdit* homeLineEdit;
						QPushButton* homeCurrentPage,* homeBlankPage,* homeDefaultPage;
				QFontComboBox* font;
			
		QWidget *connexion;
			QVBoxLayout *connexionLayout;
				QGroupBox *infoProxy;
					QFormLayout *infoProxyLayout;
						QLineEdit *hostname, *port, *user, *pass;
				QGroupBox *connexionType;
					QHBoxLayout *connexionTypeLayout;
						QRadioButton *noConn, *connHttp, *connSocks5, *connHttpCaching, *connFtpCaching;
};

#endif // SETTINGS_WINDOW_H
