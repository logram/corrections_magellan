#ifndef _MWINDOW_
#define _MWINDOW_

#include <QtWebKit>
#include <QtNetwork>

#include <QTabBar>
#include <QTabWidget>
#include <QMainWindow>
#include <QWidget>
#include <QDockWidget>
#include <QFontDialog>
#include <QInputDialog>
#include <QGroupBox>
#include <QLabel>
#include <QFontComboBox>
#include <QPrinter>
#include <QStackedWidget>
#include <QIcon>
#include <QToolBar>
#include <QAction>
#include <QMessageBox>
#include <QStatusBar>

#include <QHBoxLayout>
#include <QGridLayout>

#include <LAdressBar.h>
#include <LBookmarksManager.h>

#include "googlecompleter.h"

#include "toolbar.h"
#include "includes/settingswindow.h"

class mwindow : public QMainWindow
{
        Q_OBJECT

public:
	mwindow(); // constructeur
	QIcon favicon(QString url);
	QUrl parseUrl(QUrl);

public slots:

	// tabs and window management slots
	void changeTitle(const QString & title);
	void changeUrl(const QUrl & url);
	void changeTab(int);
	void closeTab(int);

	void newWin();

	// file slots
	void open(QUrl url);
	void openUrl();
        void openTab(bool needswitch=true,const QString& title=tr("About:New"), const QIcon& icon=QIcon());
        void closeCurrent();

	// web internal slots
	void loadingStarted();
	void loadingProgress(int);
	void loadingFinished(bool);

	// navigation slots
	void back();
	void forward();
	void refresh();
	void stop();
	void home();

	// edition slots
	void print();
	void find();
	void cut();
	void copy();
	void paste();

	void contextMenu(QPoint);

	// options slots
	void readSavedTabs();

	// help and about slots
	void about();
	void aboutLogram();

	// GENERAL TAB : SLOTS
        void openSettings();
	void defineStart(int index);
	void defineHome(QString home);
        void defineHomeCurrentPage();
	void defineHomeDefaultPage();
	void defineHomeBlankPage();
	void fontDialog();
        void defineFont(QFont);
	void defineDownloadDir(QString);

	// CONNEXION TAB : SLOTS
	void defineUseProxy(bool);
	void defineProxyType(QNetworkProxy::ProxyType);
	void defineProxyHost(QString);
	void defineProxyPort(QString);
	void defineProxyUser(QString);
	void defineProxyPass(QString);
	

	// download management slots
	void download(QNetworkReply*);
	void replyFinished(QNetworkReply*);
	
	QWebView * currentPage();

private:
	QString iconPath;
	QString downloadDir;

        QStatusBar *wstatusBar;

	// barre
	ToolBar *toolbar;

        // raccourcis
        QAction *newTabAction, *closeTabAction;

        // onglets
        QToolBar *tabsDock;
	QTabBar *tabs;
	QStackedWidget *tabsContents;
	QHBoxLayout *tabsDockLayout;
	QPushButton *newTabCornerButton;
	
	// fenetre d'options 
	QMainWindow *settingsWindow;
        QLineEdit *homeLineEdit;

	QNetworkProxy *networkProxy;
	QNetworkProxy::ProxyType networkProxyType;


	SettingsWindow* wsettings;

	// gestion des préférences (fichier de configuration et paramètres web)
	QSettings *settings;
	QWebSettings *webSettings;

protected:
	void closeEvent(QCloseEvent *event);
};

#endif
